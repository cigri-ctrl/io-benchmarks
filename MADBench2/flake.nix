{
  description = "A very basic flake";

  outputs = { self, nixpkgs }: {

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.madBench2;
    packages.x86_64-linux.madBench2 =
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "MadBench2";
        buildInputs = [ openmpi ];
        # TODO: does not work
        # src = builtins.fetchurl {
        #   url = "https://crd.lbl.gov/assets/Uploads/MADbench2.tar";
        #   sha256 = "12qhqbr0aaq3g80m2is74dxya2r3ng8diain1gj1mq20bcc1j8mq";
        # };

        src = ./.;
        installPhase = ''
          mkdir $out
          mkdir $out/bin
          mpicc -D SYSTEM -D COLUMBIA -D IO -o MADbench2.x MADbench2.c -lm
          mv MADbench2.x $out/bin
        '';
      };
  };
}

