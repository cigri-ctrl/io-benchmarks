# aio-stress


[https://www.vi4io.org/tools/benchmarks/aio-stress](https://www.vi4io.org/tools/benchmarks/aio-stress)

Benchmark which tests async io operations to a list of files

```sh
./aio-stress <filename>
```

```sh
usage: aio-stress [-s size] [-r size] [-a size] [-d num] [-b num]
                  [-i num] [-t num] [-c num] [-C size] [-nxhOS ]
                  file1 [file2 ...]
	-a size in KB at which to align buffers
	-b max number of iocbs to give io_submit at once
	-c number of io contexts per file
	-C offset between contexts, default 2MB
	-s size in MB of the test file(s), default 1024MB
	-r record size in KB used for each io, default 64KB
	-d number of pending aio requests for each file, default 64
	-i number of ios per file sent before switching
	   to the next file, default 8
	-I total number of ayncs IOs the program will run, default is run until Cntl-C
	-O Use O_DIRECT (not available in 2.4 kernels),
	-S Use O_SYNC for writes
	-o add an operation to the list: write=0, read=1,
	   random write=2, random read=3.
	   repeat -o to specify multiple ops: -o 0 -o 1 etc.
	-m shm use ipc shared memory for io buffers instead of malloc
	-m shmfs mmap a file in /dev/shm for io buffers
	-n no fsyncs between write stage and read stage
	-l print io_submit latencies after each stage
	-L print io completion latencies after each stage
	-t number of threads to run
	-u unlink files after completion
	-v verification of bytes written
	-x turn off thread stonewalling
	-h this message

	   the size options (-a -s and -r) allow modifiers -s 400{k,m,g}
	   translate to 400KB, 400MB and 400GB
version 0.21
```

