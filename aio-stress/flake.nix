{
  description = "A very basic flake";

  outputs = { self, nixpkgs }: {


    defaultPackage.x86_64-linux = self.packages.x86_64-linux.aio-stress;

    packages.x86_64-linux.aio-stress = 
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "aio-stress";
        buildInputs = [ openmpi gcc libaio ];
        phases = [ "installPhase" ];
        # src = fetchurl {
        #     url = "http://fsbench.filesystems.org/bench/aio-stress.c";
        #    sha256 = "sha256-PzLloe8K6EeUz999YL1ZWis8OZW7kb95wrlutr5+VSk=";
        # };
        src = ./.;
        installPhase = ''
          mkdir $out
          mkdir $out/bin
          gcc -Wall -laio -lpthread -o $out/bin/aio-stress ${./aio-stress.c}
        '';
      };

  };
}
